# README #


# What is this repository for? #

This is the sample Jenkins file using pipeline as a code. This pipeline require input parameter as 'FOLDERPATH' for saving auto builds.


# Prerequisite #
### This pipeline require below plugins to be installed
* GIT
* Pipeline
* Parameterized Build
* Maven
* JUnit


# How do I get set up? #

* Create new job using pipeline job.
#### General Section: ####
* Select "GitHub project" and add git URL.
* Select "This project is parameterized" and use String Parameter, in Name section put "FOLDERPATH" and folder path in Default Value.
#### Build Trigger ####
* Select "Poll SCM" and schedule the polling interval e.g "H/5 * * * *" for 5 min interval.
#### Pipeline ####
* Select "Pipeline Script" in Definition and paste the JenkinsFile script.
* Click Save

